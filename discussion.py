class SampleClass():
	def __init__(self, year):
		self.year = year

	def show_year(self):
		print(f"The year is: {self.year}")

my_obj = SampleClass(2020)

print(my_obj.year)
my_obj.show_year()

# [Section] Encapsulation
class Person():
	def __init__(self):
		self._name = "John Doe"

	# name setter
	def set_name(self, name):
		self._name = name

	# name getter
	def get_name(self):
		print(f"Name of the person: {self._name}")

	# age setter
	def set_age(self, age):
		self._age = age

	# age getter
	def get_age(self):
		print(f"The age is: {self._age}")

p1 = Person()

# print(p1.name)
p1.get_name()
p1.set_name("Bob Doe")
p1.get_name()

p1.set_age(38)
p1.get_age()

# [Section] Inheritance
class Employee(Person):
	def __init__(self, employee_id):
		super().__init__()
		self._employee_id = employee_id

	# setter method
	def set_employee_id(self, employee_id):
		self._employee_id = employee_id

    # details method
	def get_details(self):
		print(f"{self._employee_id} belongs to {self._name}")

emp1 = Employee("Emp-001")
emp1.get_details()

emp1.set_name("Bob Doe")
emp1.set_age(40)
emp1.get_details()   

class Student(Person):
    def __init__(self, studentNo, course, year_level):
        # super() can be used to invoke immediate parent class constructor
        super().__init__()
        # attributes unique to the Employee class
        self._studentNo = studentNo
        self._course = course
        self._year_level = year_level
    
    # getters
    def get_studentNo(self):
        print(f"Student number of Student is {self._studentNo}")

    def get_course(self):
        print(f"Course of Student is {self._course}")

    def get_year_level(self):
        print(f"The Year Level of Student is {self._year_level}")

    # setters
    def set_studentNo(self, studentNo):
        self._studentNo = studentNo
    
    def set_course(self, course):
        self._course = course
    
    def set_year_level(self, year_level):
        self._year_level = year_level

    # custom method
    def get_details(self):
        print(f"{self._name} is currently in year {self._year_level} taking up {self._course}.")

# Test cases:
student1 = Student("stdt-001", "Computer Science", 1)
student1.set_name("Brandon Smith")
student1.set_age(18)
student1.get_details()

# [Section] Polymorphism
# Functions and objects
class Admin():
	def is_admin(self):
		print(True)

	def user_type(self):
		print("Admin User")

class Customer():
	def is_admin(self):
		print(False)

	def user_type(self):
		print("Regular User")

def test_function(obj):
	obj.is_admin()
	obj.user_type()

user_admin = Admin();
user_customer = Customer();

test_function(user_admin)
test_function(user_customer)

# Using Classes
class TeamLead():
	def occupation(self):
		print("Team Lead")

	def has_auth(self):
		print(True)

class TeamMember():
	def occupation(self):
		print("Team Member")

	def has_auth(self):
		print(False)

tl1 = TeamLead()
tm1 = TeamMember()

for person in (tl1, tm1):
	person.occupation()
	person.has_auth()

# Using Inheritance
class Zuitt():
	def tracks(self):
		print('We are currently offering 3 tracks(developer career, pi-shape career, and short courses)')

	def num_of_hours(self):
		print('Learn the basic of web programming in 240 hours!')

class DeveloperCareer(Zuitt):
    # override the parent's num_of_hours() method
    def num_of_hours(self):
        print('Learn the basics of web development in 240 hours!')

class PiShapedCareer(Zuitt):
    # override the parent's num_of_hours() method
    def num_of_hours(self):
        print('Learn skills for no-code app development in 140 hours!')

class ShortCourses(Zuitt):
    # override the parent's num_of_hours() method
    def num_of_hours(self):
        print('Learn advanced topics in web development in 20 hours!')

course1 = DeveloperCareer()
course2 = PiShapedCareer()
course3 = ShortCourses()

for course in (course1, course2, course3):
	course.num_of_hours()

# [Section] Abstraction
from abc import ABC, abstractclassmethod

class Polygon(ABC):
	@abstractclassmethod

	def print_number_of_sides(self):
		pass 

class Triangle(Polygon):
	def __init__(self):
		super().__init__()

	def print_number_of_sides(self):
		print("This polygon has 3 sides")

class Pentagon(Polygon):
	def __init__(self):
	    super().__init__()

	# Since the Pentagon class inherited the Polygon class, it must now implement the abstract method
	def print_number_of_sides(self):
	    print(f"This polygon has 5 sides.")

shape1 = Triangle()
shape2 = Pentagon()

shape1.print_number_of_sides()
shape2.print_number_of_sides()
